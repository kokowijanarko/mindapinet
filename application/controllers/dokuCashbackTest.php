<?php
class DokuCashbackTest extends CI_Controller {
	// public $clientId = '2024';
	public $clientId = '1465';
	public $ClientSecret = '6394gqaSgFPO';
	public $sharekey = '2l5p63HjCnBK';
	public $url_doku_h2h = 'http://staging.doku.com/dokupay/h2h/';
	// public $url_doku_h2h = 'https://my.dokuwallet.com/dokupay/h2h/';

	public $sysTrace = '';
	public $senderAccountId = '1872294848';

	public function __construct(){
		parent::__construct();
		//$this->load->library('session');
	}
	
	# Copy From This START
	
	/*
	
	doku cashback parameter
	
	1 clientId | AN (128) | TRUE | Credential from DOKU
	2 accessToken | ANS (256) | TRUE Access token when sign on, from DOKU
	3 accountId | N (10) | TRUE | Set this value with dokuId or base on your unique key (email or phoneNo)
	4 transactionId | AN (32) | TRUE Unique reference id for transaction cashback from partner.
	5 amount | N (12,2) |  TRUE | Total amount cashback. Must use decimal .00 (e.g: 1000.00)
	6 remarks | AN (256) | FALSE Remarks related cashback transaction.
	7 words | ANS (256) | TRUE | Encrypted SHA1 HMAC, from element : (clientId + transactionId + systraceSignOn + sharedKey + amount )
	8 version | NS (1) | FALSE	| 1: 1.0 	2: 2.0 (Default is 1.0)
	9 responseType | N (1) FALSE | 1: JSON,	2: XML: 2,	Default is 1 (JSON)
	
	sample response
	
	{
		"dokuId":"1414150555",
		"transactionId":"INVOICE-2038",
		"amount":200000.00,
		"trackingId":"30400",
		"result":"SUCCESS",
		"dateTime":"20160414095432",
		"clientId":"2006",
		"responseCode":"0000",
		"responseMessage":{
			"id":"SUKSES",
			"en":"SUCCESS"
		}
	}
	*/
	
	public function DokuCashback() {
		//if($_POST) {
			//$data = $this->input->post();
			$systraceSignOn = $this->requestSystrace();
			$res = $this->_get_response_from_dokusignon($systraceSignOn);
			var_dump($systraceSignOn, $res);
			
			$clientId = $this->clientId;
			$accountId = 'surya@squline.com'; //$data['dokuId'];
			$accessToken = $res['accessToken'];
			$transactionId = 'TRX129MS'; //$data['transactionId'];
			$amount = '1000.00';//$data['amount'];
			$remarks = 'cashback';
			$words = $this->wordCashback($clientId, $transactionId, $systraceSignOn, $this->sharekey, $amount);
			$version = '1';
			$responseType = '1';
			
			/* $params = array(
				'clientId' => $clientId,
				'accessToken' => $accessToken,
				'accountId' => $accountId,
				'transactionId' => $transactionId,
				'amount' => $amount,
				'remarks' => $remarks,
				'words' => $words,
				'version' => $version,
				'responseType' => $responseType		
			); */
			
			$str = 'clientId='. $clientId;
			$str .= '&accessToken='. $accessToken;
			$str .= '&accountId='. $accountId;
			$str .= '&transactionId='. $transactionId;
			$str .= '&amount='. $amount;
			$str .= '&remarks='. $remarks;
			$str .= '&words='. $words;
			$str .= '&version.'. $version;
			$str .= '&responseType='. $responseType;	
			
			$result = $this->curl_doku('cashback', $str);
			header('Content-Type: application/json');
			echo json_encode($result);
			
			//$this->_logs(json_encode($result));
		// }else{
			// var_dump('asasa');
		// }
	}
	
	public function wordCashback($clientId, $transactionId, $systraceSignOn, $sharedKey, $amount){
		// clientId + transactionId + systraceSignOn + sharedKey + amount
		$concat = $clientId.$transactionId.$systraceSignOn.$sharedKey.$amount;
		return hash_hmac("sha1", $concat, $this->ClientSecret);
	}
	
	# Copy From This END
	
	public function curl_doku($url, $postfield){
		$curl = curl_init();
		curl_setopt_array($curl, array(
				CURLOPT_URL => "http://staging.doku.com/dokupay/h2h/".$url,//https://my.dokuwallet.com/dokupay/h2h/
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 300,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => $postfield,
				CURLOPT_HTTPHEADER => array(
						"cache-control: no-cache",
						"content-type: application/x-www-form-urlencoded"
				),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			return json_decode($response, TRUE);
		}
	}
	
	public function _logs($arr){
		$myfile = fopen("logs.txt", "a") or die("Unable to open file!");
		$txt = "LOG ".date('d M Y  H:i:s')." : ".$arr;
		fwrite($myfile, "\n". $txt);
		fclose($myfile);
	}
	public function requestSystrace(){
		$six_digit_random_number = mt_rand(1000000, 9999999);
		$this->sysTrace = "MS".$six_digit_random_number;
		return $this->sysTrace;
	}
	
	public function _get_response_from_dokusignon($systrace){
		return $this->DokuSignon($systrace);
	}
	
	public function DokuSignon($systrace = null) {
		$curl = curl_init();
		curl_setopt_array($curl, array(
				CURLOPT_URL => "http://staging.doku.com/dokupay/h2h/signon/" , //"https://my.dokuwallet.com/dokupay/h2h/signon",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 300,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => "clientId=".$this->clientId."&clientSecret=".$this->ClientSecret."&systrace=".$systrace."&words=".$this->words($systrace)."&version=1&responseType=1",
				CURLOPT_HTTPHEADER => array(
						"cache-control: no-cache",
						"content-type: application/x-www-form-urlencoded",
				),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			return json_decode($response, TRUE);
		}
	}
	
	public function words($systrace = null){
		$clientId = $this->clientId;
		$sharedKey = $this->sharekey;
		$ClientSecret = $this->ClientSecret;
		$concat = $clientId.$sharedKey.$systrace;
		return hash_hmac("sha1", $concat, $ClientSecret);
		//var hmac = Crypto.HMAC(Crypto.SHA1, msg, secretKey);
	}


	public function DokuCashBackNew()
	{
		if ($_POST) 
		{
			$data = $this->input->post();
			$clientId = $this->clientId;
			$accountId = $data['email']; 
			$accessToken = $res['accessToken'];
			$transactionId = $data['transactionId'];
			$amount = $data['amount'];
			$remarks = $data['remark'];
			$systraceSignOn = $this->requestSystrace();
			$inq =  $this->DokuCashBackInq($clientId,$accountId,$transactionId,$amount,$remarks, $systraceSignOn);
			if ($inq['result']['responseCode'] == 0000) {
				$arr = $inq ;
				header('Content-Type: application/json');
				echo json_encode($arr);
			}else{
				$arr = array(
					'responseCode' => $inq['result']['responseCode'],
					'status' => 'failed'
					'message' => $inq['result']['responseMessage']['id']
				);
				header('Content-Type: application/json');
				echo json_encode($arr);
			}

		}
	}

	public function DokuCashBackInq($clientId,$accountId,$transactionId,$amount,$remarks, $systraceSignOn= null)
	{
			$res = $this->_get_response_from_dokusignon($systraceSignOn);

			$postfield = "clientId=".$clientId;
			$postfield .= "&accessToken=".$res['accessToken'];
			$postfield .= "&accountId=".$accountId;
			$postfield .= "&transactionId=".$transactionId;
			$postfield .= "&amount=".$amount;
			$postfield .= "&remark=".$remark;
			$postfield .= "&words=".$this->wordCashBackNew($clientId,$transactionId,$systraceSignOn,$this->sharedkey,$amount);
			$postfield .= "&version=1&responseType=1";
			$result = $this->curl_doku('cashback', $postfield);
		return $result;

	}

	public function wordCashBackNew($clientId,$transactionId,$systraceSignOn, $sharedKey, $amount)
	{
		$concat = $clientId.$systraceSignOn.$transactionId.$sharedKey.$amount;
		return hash_hmac("sha1", $concat, $this->ClientSecret);
	}

}
?>
