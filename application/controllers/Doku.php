<?php
/*	@Auhtor : Diko
 *	DOKU H2H API Development
* - Systrace harus di request baru ketika ingin melakukan akatifitas ke API untuk melakukan sign on.
* - Systrace yang di request untuk sign on di panggil kembali untuk melakukan call API yang lain.
*
*/

class Doku extends CI_Controller {
	public $clientId = '1465';
	public $ClientSecret = '6394gqaSgFPO';
	public $sharekey = '2l5p63HjCnBK';
	//public $url_doku_h2h = 'http://staging.doku.com/dokupay/h2h/';
	public $url_doku_h2h = 'https://my.dokuwallet.com/dokupay/h2h/';

	public $sysTrace = '';
	public $senderAccountId = '1872294848';

	public function __construct(){
		parent::__construct();
		//$this->load->library('session');
	}

	public function renderWebview(){

		$data['dokuId'] = '1457238944';
		$data['customerEmail'] = 'diko@mymindstores.com';
		$data['customerName'] = 'DIKO BAGDA ANGGARA';
		$data['customerPhone'] = '081219242434';
		$data['systraceSignOn'] = $this->requestSystrace();

		if(!empty($data['dokuId'])) {
			// hit ke check balance
			$res = $this->DokuCheckBalance($data['customerEmail'], $data['systraceSignOn']);
			$data['lastBalance'] = $this->convert_to_rupiah($res['lastBalance']);

		}

		$this->load->view('redeem_point/index', $data);
	}

	public function render() {
		if ($_POST) {
			$postdata = $this->input->post();
			$data = array (
					'dokuId' => $postdata['dokuId'],
					'customerName' => $postdata['customerName'],
					'customerEmail' => $postdata['customerEmail'],
					'customerPhone' => $postdata['customerPhone'],
					'systraceSignOn' => $this->requestSystrace()
			);

			if(!empty($data['dokuId'])) {
				// hit ke check balance
				$res = $this->DokuCheckBalance($data['customerEmail'], $data['systraceSignOn']);
				$data['lastBalance'] = $this->convert_to_rupiah($res['lastBalance']);

			}
			$this->load->view('redeem_point/index', $data);
		}
	}

	public function destroy(){
		$this->session->sess_destroy();
	}


	public function DokuRegisterCustomer(){
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$customerEmail = $this->input->post('customerEmail');
			$customerName =  $this->input->post('customerName');
			$customerPhone =  $this->input->post('customerPhone');
			$systraceSignOn =  $this->input->post('systraceSignOn');

			/* 	Setelah mendapatkan parameter, yang dilakukan pertama adalah signOn untuk mendapatkan accessToken
			 * 	function _get_response_from_dokusignon($systraceSignOn);
			 */
			 
			$res = $this->_get_response_from_dokusignon($systraceSignOn);
			//print_r($res);
			$data = array(
					'customerEmail' => $customerEmail,
					'customerName' => $customerName,
					'customerPhone' => $customerPhone,
					'systraceSignOn' => $systraceSignOn,
					'accessToken' => $res['accessToken'],
					'clientId' => $this->clientId,
					'words' => $this->wordRegister($this->clientId, $this->sharekey, $customerEmail, $systraceSignOn)
			);

			$postfield = "clientId=".$this->clientId;
			$postfield .= "&accessToken=".$data['accessToken'];
			$postfield .= "&customerName=".$data['customerName'];
			$postfield .= "&customerEmail=".$data['customerEmail'];
			$postfield .= "&customerPhone=".$data['customerPhone'];
			$postfield .= "&words=".$data['words'];
			$postfield .= "&version=1&responseType=1";

			$response = $this->curl_doku('signup', $postfield);

			//print_r($response);

			if (!empty($response['dokuId'])) {
				//update doku id
				//echo $response['dokuId'];
				$update = $this->updateDokuId($response['customer']['email'], $response['dokuId']);
				if($update == "sukses") {
					$data = array (
							'dokuId' => $response['dokuId'],
							'customerName' => $customerName,
							'customerEmail' => $customerEmail,
							'customerPhone' => $customerPhone,
							'systraceSignOn' => $this->requestSystrace()
					);

					if(!empty($data['dokuId'])) {
						// hit ke check balance
						$res = $this->DokuCheckBalance($data['customerEmail'], $data['systraceSignOn']);
						$data['lastBalance'] = $this->convert_to_rupiah($res['lastBalance']);

					}
					$this->load->view('redeem_point/index', $data);
				} else {
					exit();
				}
			}

		}
	}

	public function DokuCheckBalance($customerEmail, $systraceSignOn = null){
		$res = $this->_get_response_from_dokusignon($systraceSignOn);

		//print_r($res);die();

		$postfield = "clientId=".$this->clientId;
		$postfield .= "&accessToken=".$res['accessToken'];
		$postfield .= "&accountId=".$customerEmail;
		$postfield .= "&words=".$this->wordCheckBalance($this->clientId, $systraceSignOn, $this->sharekey, $customerEmail);
		$postfield .= "&version=1&responseType=1";
		$result = $this->curl_doku('custsourceoffunds', $postfield);
		return $result;
	}

	public function DokuTransfer() {
		if($_POST) {
			$data = $this->input->post();
			$senderAccountId = $this->senderAccountId;
			$receiverAccountId = $data['receiverAccountId']; //'1457238944';
			$amount = $data['amount'];//'1000.00';
			$transactionId = $data['transactionId'];//'TRX129MS';
			$systraceSignOn = $this->requestSystrace();
			$inq = $this->DokuTransferInq($senderAccountId, $receiverAccountId, $amount, $transactionId, $systraceSignOn);

			if ($inq['result']['responseCode'] == 0000) {
				// hit to confirm payment
				$arr = $this->DokuTransferPay($inq['accessToken'], $inq['result']['trackingId'], $systraceSignOn);

				header('Content-Type: application/json');
				echo json_encode($arr);
			} else {
				$arr = array(
					'responseCode' => $inq['result']['responseCode'],
					'status' => 'failed',
					'message' => $inq['result']['responseMessage']['id']
				);
				header('Content-Type: application/json');
				echo json_encode($arr);
			}
			$this->_logs(json_encode($arr));
		}
	}

	public function DokuTransferInq($senderAccountId, $receiverAccountId, $amount, $transactionId, $systraceSignOn = null) {
		$res = $this->_get_response_from_dokusignon($systraceSignOn);

		$postfield = "clientId=".$this->clientId;
		$postfield .= "&accessToken=".$res['accessToken'];
		$postfield .= "&senderAccountId=".$senderAccountId;
		$postfield .= "&receiverAccountId=".$receiverAccountId;
		$postfield .= "&transactionId=".$transactionId;
		$postfield .= "&amount=".$amount;
		$postfield .= "&words=".$this->wordTransferInq($senderAccountId, $this->clientId, $transactionId, $systraceSignOn, $this->sharekey, $receiverAccountId, $amount);

		$postfield .= "&version=1&responseType=1";
		$result = $this->curl_doku('sendmoney/init', $postfield);
		$res = array(
				'result' => $result,
				'accessToken' => $res['accessToken']
		);
		return $res;

	}

	public function DokuTransferPay($accessToken, $trackingId, $systraceSignOn) {

		$postfield = "clientId=".$this->clientId;
		$postfield .= "&accessToken=".$accessToken;
		$postfield .= "&trackingId=".$trackingId;
		$postfield .= "&words=".$this->wordTransferPay($trackingId, $this->clientId, $systraceSignOn, $this->sharekey);

		$postfield .= "&version=1&responseType=1";
		$result = $this->curl_doku('sendmoney/pay', $postfield);
		return $result;

	}

	public function DokuCashBackNew()
	{
		if ($_POST) 
		{
			$data = $this->input->post();
			//var_dump($data);die();
			$clientId = $this->clientId;
			$accountId = $data['email']; 
			$transactionId = $data['transactionId'];
			$amount = $data['amount'];
			$remarks = $data['remark'];
			$systraceSignOn = $this->requestSystrace();
			$inq =  $this->DokuCashBackInq($clientId,$accountId,$transactionId,$amount,$remarks, $systraceSignOn);
			//var_dump($inq['responseCode']);die();
			if ($inq['responseCode'] == 0000) {
				$arr = $inq ;
				header('Content-Type: application/json');
				echo json_encode($arr);
			}else{
				$arr = array(
					'responseCode' => $inq['responseCode'],
					'status' => 'failed',
					'message' => $inq['responseMessage']['id']
				);
				header('Content-Type: application/json');
				echo json_encode($arr);
			}
			$this->_logs(json_encode($arr));
		}
	}

	public function DokuCashBackInq($clientId,$accountId,$transactionId,$amount,$remarks, $systraceSignOn= null)
	{
			$res = $this->_get_response_from_dokusignon($systraceSignOn);

			$postfield = "clientId=".$clientId;
			$postfield .= "&accessToken=".$res['accessToken'];
			$postfield .= "&accountId=".$accountId;
			$postfield .= "&transactionId=".$transactionId;
			$postfield .= "&amount=".$amount;
			$postfield .= "&remark=".$remarks;
			$postfield .= "&words=".$this->wordCashBackNew($clientId,$transactionId,$systraceSignOn,$this->sharekey,$amount);
			$postfield .= "&version=1&responseType=1";
			$result = $this->curl_doku('cashback', $postfield);
		return $result;

	}

	public function _logs($arr){
		$myfile = fopen("logs.txt", "a") or die("Unable to open file!");
		$txt = "LOG ".date('d M Y  H:i:s')." : ".$arr;
		fwrite($myfile, "\n". $txt);
		fclose($myfile);
	}



	public function requestSystrace(){
		$six_digit_random_number = mt_rand(1000000, 9999999);
		$this->sysTrace = "MS".$six_digit_random_number;
		return $this->sysTrace;
	}

	public function requestOTP(){
		$six_digit_random_number = mt_rand(100000, 999999);
		return $six_digit_random_number;
	}


	public function DokuSignon($systrace = null) {
		$curl = curl_init();
		curl_setopt_array($curl, array(
				CURLOPT_URL => "https://my.dokuwallet.com/dokupay/h2h/signon",//http://staging.doku.com/dokupay/h2h/signon
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => "clientId=".$this->clientId."&clientSecret=".$this->ClientSecret."&systrace=".$systrace."&words=".$this->words($systrace)."&version=1&responseType=1",
				CURLOPT_HTTPHEADER => array(
						"cache-control: no-cache",
						"content-type: application/x-www-form-urlencoded",
				),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			return json_decode($response, TRUE);
		}
	}



	public function curl_doku($url, $postfield){
		$curl = curl_init();
		curl_setopt_array($curl, array(
				CURLOPT_URL => "https://my.dokuwallet.com/dokupay/h2h/".$url,//http://staging.doku.com/dokupay/h2h/
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => $postfield,
				CURLOPT_HTTPHEADER => array(
						"cache-control: no-cache",
						"content-type: application/x-www-form-urlencoded"
				),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			return json_decode($response, TRUE);
		}
	}

	public function _get_response_from_dokusignon($systrace){
		return $this->DokuSignon($systrace);
	}

	public function wordCheckBalance($clientId, $systraceSignOn, $sharedkey, $customerEmail){
		$concat = $clientId.$systraceSignOn.$sharedkey.$customerEmail;
		return hash_hmac("sha1", $concat, $this->ClientSecret);
	}

	public function wordRegister($clientId = null, $sharedKey = null, $customerEmail = null, $systraceSignOn = null){
		$concat = $clientId.$sharedKey.$customerEmail.$systraceSignOn;
		return hash_hmac("sha1", $concat, $this->ClientSecret);
	}

	public function wordTransferInq($senderAccountId, $clientId, $transactionId, $systraceSignOn, $sharedKey, $receiverAccountId, $amount){
		// senderAccountId + clientId + transactionId + systraceSignOn + sharedKey + receiverAccountId + amount
		$concat = $senderAccountId.$clientId.$transactionId.$systraceSignOn.$sharedKey.$receiverAccountId.$amount;
		return hash_hmac("sha1", $concat, $this->ClientSecret);
	}

	public function wordTransferPay($trackingId, $clientId, $systraceSignOn, $sharekey) {
		$concat = $trackingId.$clientId.$systraceSignOn.$sharekey;
		return hash_hmac("sha1", $concat, $this->ClientSecret);
	}
	
	public function wordCashBackNew($clientId,$transactionId,$systraceSignOn, $sharedKey, $amount)
	{
		$concat = $clientId.$transactionId.$systraceSignOn.$sharedKey.$amount;
		return hash_hmac("sha1", $concat, $this->ClientSecret);
	}

	public function words($systrace = null){
		$clientId = $this->clientId;
		$sharedKey = $this->sharekey;
		$ClientSecret = $this->ClientSecret;
		$concat = $clientId.$sharedKey.$systrace;
		return hash_hmac("sha1", $concat, $ClientSecret);
		//var hmac = Crypto.HMAC(Crypto.SHA1, msg, secretKey);
	}

	public function convert_to_rupiah($angka)
	{
		return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
	}

	public function sendOTP(){
		$this->load->library('email');
		$code = $this->requestOTP();

		$customerEmail = $this->input->post('customerEmail');
		$config['protocol']    = 'smtp';
		$config['smtp_host']    = 'ssl://smtp.gmail.com';
		$config['smtp_port']    = '465';
		$config['smtp_user']    = 'astrabuddies@gmail.com';
		$config['smtp_pass']    = 'astrabuddies123456';
		$config['charset']    = 'utf-8';
		$config['newline']    = "\r\n";
		$config['mailtype'] = 'html'; // or html
		$config['validation'] = TRUE; // bool whether to validate email or not
		$this->email->initialize($config);

		$this->email->from('astrabuddies@gmail.com', 'Astralife Buddies');
		$this->email->to($customerEmail);

		$this->email->subject('Kode Verifikasi');
		$this->email->message('Kode verifikasi anda adalah : '.$code);

		if ( ! $this->email->send())
		{
			$arr = array(
					'message'=>'failed',
					'code'=> "00"
			);
			header("Access-Control-Allow-Origin: *");
			header('Content-Type: application/json');
			echo json_encode($arr);
		} else {
			$arr = array(
					'message'=>'sukses',
					'code'=> $code //sementara di tampung di variable, nantinya insert ke server
			);
			header("Access-Control-Allow-Origin: *");
			header('Content-Type: application/json');
			echo json_encode($arr);
		}
	}

	public function updateDokuId($email = NULL, $dokuID = NULL) {
		//$email = 'dev.dikobagda@gmail.com';
		//$dokuID = '123456';
		$data = array('email'=>$email,'dokuID'=>$dokuID);
		//$data_json = json_encode($data);

		//$response = $this->getBuddiesToken($email);
		//$object = json_decode($response, true);
		$curl = curl_init();

		curl_setopt_array($curl, array(
				CURLOPT_URL => "http://mndsvr2.net/api/profil_user_dokuid",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => $data,
				CURLOPT_HTTPHEADER => array(
						"x-api-key: dmDWaFxqHPuagnMT9gIK"
				),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			return 'sukses';
		}
	}

	public function getBuddiesToken($email) {
		//uat.mndsvr2.net/api/oauth2_token
		$postfield = array(
          "grant_type" => 'client_credentials',
          "client_id"=>$email,
          "client_secret"=>'password',
          "username"=>$email,
		  "scope"=>'default'
		);


		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://mndsvr2.net/api/oauth2_token",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $postfield,
		  CURLOPT_HTTPHEADER => array(
			"x-api-key: dmDWaFxqHPuagnMT9gIK"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}

	}




	public function test() {
		$this->load->library('email');
		$config['protocol']    = 'smtp';
		$config['smtp_host']    = 'ssl://smtp.gmail.com';
		$config['smtp_port']    = '465';
		$config['smtp_user']    = 'astrabuddies@gmail.com';
		$config['smtp_pass']    = 'astrabuddies123456';
		$config['charset']    = 'utf-8';
		$config['newline']    = "\r\n";
		$config['mailtype'] = 'html'; // or html
		$config['validation'] = TRUE; // bool whether to validate email or not

		$this->email->initialize($config);

		$this->email->from('astrabuddies@gmail.com', 'myname');
		$this->email->to('diko@mymindstores.com');

		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');

		if ( ! $this->email->send()) {
			echo "failed";
		} else {
			echo "sukses";
		}

	}





}
