<?php

class Logs extends CI_Controller {
	
	public function index() {
		$this->load->view('logs/logs_view');
	}
	
	public function esublogs(){
		
		$json = file_get_contents('php://input');
		$data = json_decode($json);
		$myfile = fopen("esub/casedata_".$data->quotation->ereference_number.".json", "w") or die("Unable to open file!");
		$txt = $json;
		fwrite($myfile, "\n". $txt);
		fclose($myfile);
	
	}
	
	public function detail($filename){
		$string = file_get_contents("./esub/".$filename);
		$json_a = json_decode($string, true);
		echo "<a href='http://mymindstores.com/api/esub/".$filename."'><button class='btn btn-default'>JSON</button></a>";
		print_r('<pre>');
		print_r($json_a);
		print_r('</pre>');
	}
	
	
	public function random(){
		$six_digit_random_number = mt_rand(100, 999);
		return $six_digit_random_number;
	}
}