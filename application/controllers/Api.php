<?php

class Api extends MY_Controller {
	var $json_getproduct = 'http://localhost:8080/astralife-middleware/json/get_product.json';
	var $get_token = 'https://api-uat.astralife.co.id/token'; 
	public function __construct(){
		
		parent::__construct();
		//$this->load->helper('astralife_helper');
		
	}
	
	public function index() {
		$this->load->view('api_tools/index');
	}
	
	function curl_astralife ($URL = NULL, $postfield, $headers) {
		$curl = curl_init();
	
		curl_setopt_array($curl, array(
		CURLOPT_URL => $URL,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_POSTFIELDS => $postfield,
		CURLOPT_HTTPHEADER => $headers,
		));
	
		return curl_exec($curl);
		/*
			$err = curl_error($curl);
		curl_close($curl);
	
		if ($err) {
		echo "cURL Error #:" . $err;
		} else {
		echo $response;
		}
		*/
	}
	
	public function getToken(){
		$postfield = "username=dikobagda&password=milikW1R&grant_type=password";
		$headers = array(
					"authorization: Basic QVZMenFCbF9kVXVoVlFOTnJFUmNhNDJtOWdrYTppYmFYRVIxUFlQem03eWRnYWhjY1FmVlZlOTBh",
					"cache-control: no-cache",
					"content-type: application/x-www-form-urlencoded",
					"postman-token: fddc4f31-8c8f-11a4-7052-6d13eddab64f"
				);
		$result = $this->curl_astralife('https://api-uat.astralife.co.id/token', $postfield, $headers);
		$obj = json_decode($result);
		//$this->_print_data($obj->access_token);
		return $obj->access_token;
	}
	
	public function getProduct() {
		//echo "Access_token : ".$this->getToken()."<br />";
		$this->load->library('form_validation');
		$this->form_validation->set_rules('agentID', 'agentID', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			echo "field agentID can not be empty"; die();
		}
		else
		{
			$agentID = $this->input->post('agentID');
			
			$postfield = '{"agent_code":"'.$agentID.'"}';
			//$this->_print_data("PARAMETER (MANDATORY FIELD)", json_decode($postfield));
			$headers = array(
						"accept: application/json",
						"authorization: Bearer ".$this->getToken(),
						"cache-control: no-cache",
						"content-type: application/json",
						"postman-token: a0589194-6fb9-c6de-e5f5-7db85fd47a3c"
					);
			$result = $this->curl_astralife('https://api-uat.astralife.co.id/ec/v1/GetProduct', $postfield, $headers);
			$data['obj'] = json_decode($result);
			
			$this->load->view('api_tools/getProduct',$data);
			//$this->_print_data("RESULT", $data['obj']);
		}
	}
	
	public function getPackage(){
		$data = $this->input->post();
		/*
		print ('<pre>');
		print_r($data);
		print ('</pre>');
		*/
		$jsonData = json_decode(json_encode($data), FALSE);
		//print_r(json_encode($jsonData));
		
		$postfield = json_encode($jsonData);
		
		/* Default parameter from astralife API documentation /*
		/*
		$postfield = '{
					 "agent_code":"00000458",
					 "product_code": "FPA",
					 "options": [
					    {
					        "op_code": "ROP1",
					        "op_value": "Y"
					    }
					 ],
					 "cust_info": [
					        {
					           "dob": "1973-12-31",
					           "gender_code": "F",
					           "rel_code": null,
					           "role_code": "4",
					           "life_index": null,
					           "rider": []  
					        },
					        {
					           "dob": "1973-12-31",
					           "gender_code": "F",
					           "rel_code": "SE",
					           "role_code": "8",
					           "life_index": 1,
					           "rider": []
					        }
					    ]
					}';
		*/
		
		//$this->_print_data("PARAMETER (MANDATORY FIELD)", json_decode($postfield));
		$headers = array(
				"accept: application/json",
				"authorization: Bearer ".$this->getToken(),
				"cache-control: no-cache",
				"content-type: application/json",
				"postman-token: a0589194-6fb9-c6de-e5f5-7db85fd47a3c"
		);
		$result = $this->curl_astralife('https://api-uat.astralife.co.id/ec/v1/GetPackage', $postfield, $headers);
		$data['obj'] = json_decode($result);
		$this->load->view('api_tools/getPackage', $data);
		//$obj = json_decode($result);
		//$this->_print_data("RESULT", $obj);
	}
	
}

