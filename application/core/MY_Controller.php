<?php
class MY_Controller extends CI_Controller {
	var $url_get_product = '';
	
	function _get_post_data()
	{
		if (isset($_POST))
		{
			$val_submit = array();
			foreach($_POST as $idx => $val){
				if(is_array($val)){
					foreach($val as $vidx=>$vval)
					$val_submit[$idx][$vidx] = (string)$val[$vidx];
				}
				else
				$val_submit[$idx] = $this->input->post($idx);

				/*	if($val_submit[$idx]==''){
				 $error = array(
				 "$idx" => "$idx"
				 );
					$this->_flashMsg($error);
					redirect('members/register');
					}
					*/
			}
			return $val_submit;
		}

		return false;
	}
	

	function _flashMsg($error){
		$this->session->set_flashdata($error);
	}

	function _print_data($header,$data){
		
		print '<div style="background:#262725; color:#11ff65; padding:20px; border-top:3px solid #11ff65; border-bottom:3px solid #11ff65;">';
		print '<pre>';
		echo '<h1>'.$header.'</h1>';
		print_r($data);
		print '<pre>';
		print '</div>';
	}
}
?>