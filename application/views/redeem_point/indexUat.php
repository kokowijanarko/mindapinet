<!DOCTYPE html>
<html lang="en">
  <head>
  	<title>Redeem Point</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  </head>


  <body>

		<div class="container">
		  	<?php $this->load->view('redeem_point/pageUat')?>
		</div>

  </body>
  <script type="text/javascript">
  $(function() {

	  var tempCode = '';
	  var counterBool = true;

	    $('#login-form-link').click(function(e) {
			$("#DOKUConnect").delay(100).fadeIn(100);
			$("#DOKUCheckBalance").delay(100).fadeIn(100);
	 		$("#register-form").fadeOut(100);
			$('#register-form-link').removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});
		$('#register-form-link').click(function(e) {
			$("#register-form").delay(100).fadeIn(100);
	 		$("#DOKUConnect").fadeOut(100);
	 		$("#DOKUCheckBalance").delay(100).fadeOut(100);
			$('#login-form-link').removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});


		$(document).ready(function(){
			// button request OTP via email
			var getEmail = $("#customerEmail").val();

			$("#getCode").on('click',function(){
				// set Timer
				timer();
				// send mail
				$.ajax({
		            type: "POST",
		            url: "http://mindapi.net/dokuproduction/sendOTP",
		            data: { customerEmail: getEmail },
		            success: function(data){


						var msg = JSON.stringify(data.message);
					 	var message = new String();
					 	message = msg.toString().replace(/"/g, "");
			            //message seharusnya klo sukses
						if( message == "sukses" ) {
				            tempCode = JSON.stringify(data.code);

				            var eventstring = new String();
				            eventstring = tempCode.toString().replace(/"/g, "");
							$('.msg').text(tempCode);
				         	if(eventstring == "00"){
				         		tempCode = eventstring;
				         	} else {
				         		tempCode = eventstring;
				         	}

				            $('.msg').fadeIn(3000);
				           	 	$('.msg').text('Message : Mohon segera masukkan kode verifikasi yang terkirim ke email anda');
				            }
			            }
		        });
			});

			$("#DOKUConnect").submit(function(e){
				var verificationCode = $('#verificationCode').val();

				if (verificationCode == "") {
					$('.msg').text('Message : Kolom kode verifikasi harap diisi');
					e.preventDefault();
				} else {

					if (counterBool == false) {
						$('.msg').text('Message : Waktu anda habis, Silahkan dapatkan kode kembali');
						e.preventDefault();
					} else {
						if (tempCode == verificationCode) {
							//submit
							console.log('submitted');
							//e.preventDefault();
						} else {
							//$('.msg').text('Message : Kode verifikasi salah. code adalah ' + tempCode);
							e.preventDefault();
						}
					}
				}
			});

		});


		function timer(){

		      var seconds = 300;
		      var second = 0;
		      var interval;


		      interval = setInterval(function() {

		          setTimerInTheBox("Verifikasi Timeout "+ (seconds - second) +" detik");
		          counterBool = true;
		          if (second >= seconds) {
		        	  setTimerDie('Dapatkan Kode');
		        	  tempCode = '';
		        	  $('.msg').text('Message : Waktu anda sudah habis, Silahkan dapatkan kode kembali');
		              clearInterval(interval);
		          }

		          second++;
		      }, 1000);
	    }

		function setTimerInTheBox(counter){
			$("#getCode").text(counter);
		}

		function setTimerDie(text){
			$("#getCode").text(text);
		}
	});


  </script>
</html>
