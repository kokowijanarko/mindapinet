<style>
body {
    padding-top: 90px;
    font-family: 'Roboto', sans-serif;
}
label {
	padding: 10px 0 10px 4px;
}
.panel-login>.panel-heading {
	color: #00415d;
	background-color: #fff;
	border-color: #fff;
	text-align:center;
}
.panel-login>.panel-heading a{
	text-decoration: none;
	color: #666;
	font-weight: bold;
	font-size: 15px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login>.panel-heading a.active{
	color: #029f5b;
}
.panel-login>.panel-heading hr{
	margin-top: 10px;
	margin-bottom: 0px;
	clear: both;
	border: 0;
	height: 1px;
	background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
	background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
}
.panel-login input[type="text"], .panel-login input[type="number"], .panel-login input[type="email"],.panel-login input[type="password"] {
	height: 45px;
	border: 1px solid #ddd;
	font-size: 16px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login input:hover,
.panel-login input:focus {
	outline:none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
	border-color: #ccc;
}
.btn-redeem {
	background-color: transparent;
	outline: none;
	color: red;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: red;
}
.btn-redeem:hover,
.btn-redeem:focus {
	color: #ccc;
	background-color: transparent;
	border-color: #53A3CD;
}

.btn-verify {
	background-color: red;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 10px 0;
	text-transform: uppercase;
	border-color: #59B2E6;
}

.forgot-password {
	text-decoration: underline;
	color: #888;
}
.forgot-password:hover,
.forgot-password:focus {
	text-decoration: underline;
	color: #666;
}

.btn-register {
	background-color: #1CB94E;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #1CB94A;
}
.btn-register:hover,
.btn-register:focus {
	color: #fff;
	background-color: #1CA347;
	border-color: #1CA347;
}

#DOKUCheckBalance {
	width: auto;
	radius: 5px;
	height: 200px;
	color: #fff;
	background: red;
	
 	-moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -khtml-border-radius: 5px;
    border-radius: 5px;
    padding: 20px;
}

</style>

<?php
	$menu_active_1=null;
	$menu_active_2=null;
	
	$display_none_2 = 'display: none;';
	$display_none_3 = 'display: none;';
	if (!empty($dokuId)) {
		$display_none_3 = 'display: block';
	} else {
		$menu_active_2 = 'active';
		$display_none_2 = 'display: block;';
		
	}
?>

<div class="container">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							
							<div class="col-xs-12">
								<a href="#" class="<?php echo $menu_active_2;?>" id="login-form-link">DOKU Wallet</a>
							</div>
							
						</div>
					<hr>
				</div>

				<div class="panel-body">
					<div class="row">
						<!-- DOKU Connect -->
						
						<div class="col-lg-12">
							<?php if (empty($dokuId)) {?>
								
								<?php $attr = array('id'=> 'DOKUConnect', 'role'=>'form', 'style'=>$display_none_2); ?>
								<?php echo form_open('dokuproduction/DokuRegisterCustomer', $attr); ?>
										<label>Untuk meredeem point anda, pertama connect email anda dengan DOKU Wallet</label>
										<div class="form-group">
											<input type="hidden" name="systraceSignOn" id="systraceSignOn" tabindex="2" class="form-control" value="<?php echo $systraceSignOn;?>">
											<input type="hidden" name="customerName" id="customerName" tabindex="2" class="form-control" value="<?php echo $customerName;?>">
											<input type="hidden" name="customerPhone" id="customerPhone" tabindex="2" class="form-control" value="<?php echo $customerPhone;?>">
											<input type="email" name="customerEmail" id="customerEmail" tabindex="2" class="form-control" placeholder="Email DOKU Account" value="<?php echo $customerEmail;?>" >
											
										</div>
	
										<div class="input-group">
	               							<input type="number" tabindex="3" class="form-control" size="6"  placeholder="Kode Verifikasi" id="verificationCode">
							               	<span class = "input-group-btn">
								                  <button class = "btn btn-default" id="getCode" type = "button" style="height: 45px; background: red; color: #fff; border: 1px solid red;">
							                      Dapatkan Kode
							                  </button>
						                  	</span>
										</div>
										<div>
											<label class="msg"></label>
										</div>
	
									<div class="form-group" style="padding-top: 20px;">
										<div class="row">
											<div class="col-sm-12">
												<button type="submit" id="btn-connectDoku" tabindex="5" class="form-control btn btn-redeem" > Hubungkan DOKU Account</button>
											</div>
										</div>
									</div>
	
								<?php echo form_close(); ?>

							<?php } else { ?>
								<div id="DOKUCheckBalance" style="<?php echo $display_none_3;?>">
									<table class="tbl_balance" width="70%">
										<tr>
											<td width="30%"> DOKU ID </td>
											<td> : &nbsp; </td>
											<td> <?php echo $dokuId; ?> </td>
										</tr>
										<tr>
											<td> Email </td>
											<td> : </td>
											<td> <?php echo $customerEmail; ?> </td>
										</tr>
										
										<tr>
											<td> Last Balance </td>
											<td> : </td>
											<td> <?php if(isset($lastBalance)){echo $lastBalance;} ?> </td>
										</tr>
									</table>
								</div>
							<?php } ?>
							
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
