<!DOCTYPE html>
<html>
<head>
	<title>Log casedata</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
	
</head>
<body>
<div class="container" style="padding-top: 50px">
	<table id="myTable" class="display" cellspacing="0" width="100%" > 
		<thead>
			<tr>
				<th width="5%"> No </th>
				<th> Date </th>
				<th> Filename </th>
				<th> Action </th>
			</th>
		</thead>
		<tbody>
		<?php
			$i = 1;
			foreach(glob('./esub/*.json') as $filename){
				if (file_exists($filename)) {
					$date = date ("F d Y H:i:s.", filemtime($filename));
					$filename = explode("/", $filename);
				}
				
				echo '<tr>';
				 echo '<td>'.$i.'</td>';
				 echo '<td>'.$date.'</td>';
				 echo '<td>'.$filename[2].'</td>';
				 echo '<td><a target="_blank" href="http://mymindstores.com/api/logs/detail/'.$filename[2].'"><button class="btn btn-success">View</button></a></td>';
				echo '</tr>';
				 $i++;
			 }
		?>
		</tbody>
	</table>
	
</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

<script>
$(document).ready(function() {
    $('#myTable').DataTable({
		"pageLength": 10,
		"order": [[ 1, "desc" ]]
	});
} );
</script>
</body>

</html>