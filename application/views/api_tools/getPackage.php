<!DOCTYPE html>
<html lang="en">
<head>
  <title>Mindstores API</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h3>Package</h3>
  <!-- Nav tabs -->
  <?php $length = sizeof($obj->packages);?>
  <ul class="nav nav-tabs" role="tablist">
    <?php for($i=0;$i<$length;$i++) {?>
    	<?php if ($i==1) {?>
    		<?php $active = 'active'?>
    	<?php } else { ?>
    		<?php $active = '';?>
    	<?php }?>
    	<li role="presentation" class="<?php echo $active;?>"><a href="#package_<?php echo $i;?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $obj->packages[$i]->package_name;?></a></li>
    <?php } ?>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
  	<?php for($i=0;$i<$length;$i++) {?>
    	<?php if ($i==1) {?>
    		<?php $active = 'active'?>
    	<?php } else { ?>
    		<?php $active = '';?>
    	<?php }?>
    	<div role="tabpanel" class="tab-pane <?php echo $active;?>" id="package_<?php echo $i?>">
    		<table class="table" style="margin-top: 15px;">
    			<tr> 
    				<td width="50%">Package Name</td>
    				<td>:</td>
    				<td><?php echo $obj->packages[$i]->package_name;?></td> 
    			</tr>
    			
    			<tr> 
    				<td>Package Code</td>
    				<td>:</td>
    				<td><?php echo $obj->packages[$i]->package_code;?></td> 
    			</tr>
    			
    			<tr> 
    				<td>Package premium per life</td>
    				<td>:</td>
    				<td><?php echo convert_to_rupiah($obj->packages[$i]->package_premium_per_life);?></td> 
    			</tr>
    			
    			<tr> 
    				<td>Uang pertanggungan</td>
    				<td>:</td>
    				<td><?php echo convert_to_rupiah($obj->packages[$i]->package_sum_assured);?></td> 
    			</tr>
    			
    			<tr> 
    				<td>Pengembalian premi</td>
    				<td>:</td>
    				<td><?php echo convert_to_rupiah($obj->packages[$i]->package_rop);?></td> 
    			</tr>
    			
    			<tr>
    				<td colspan="3"> 
    					<div class="">
			    			<h3>eligible_bill_freq</h3>
			    			<?php foreach($obj->packages[$i]->eligible_bill_freq as $val) { ?>
			    				<input type="radio" name="eligible_bill_freq" value="<?php echo $val->bf_name;?>">
			    				<?php echo $val->bf_name." : ".convert_to_rupiah($val->bf_premium_per_life);?>
			    				<br />
			    			<?php } ?>
			    		</div>
    				</td>
    			</tr>
    			<tr>
    				<td>
    					<button type="button" class="btn btn-success"> Next </button>
    				</td>
    			</tr>
    		</table>
    		
    		
    	</div>
    <?php } ?>
  </div>
  
  
  
  <?php echo "<pre>";?>
  <?php print_r($obj);?>
  <?php echo "</pre>";?>
  
</div>
<?php 
function convert_to_rupiah($angka)
{
	return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
}
?>
</body>
</html>
