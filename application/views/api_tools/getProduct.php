<html>
	<head>
		<title>Form</title>
		<?php include 'include.php';?>
	</head>
	<style>
		.cust_info { border: 1px solid black; padding: 30px; margin-bottom: 30px; margin-top: 20px; display: none;}
		.choose_product { border: 1px solid black; padding: 30px; margin-bottom: 30px; margin-top: 20px; }
	</style>
	<body>
		<div class="container">
		<div class="message"></div>
		</div>
		<?php $length = sizeof($obj->products);?>
		<?php $attr = array('id'=>'form_open');?>
		<?php echo form_open('api/getPackage');?>
		<div class="container choose_product" >
			<input type="hidden" name="agent_code" value="00000749"> 
			<div class="col-md-12">
				<h1>Choose Product</h1>
				<?php for($i=0;$i<$length;$i++) { ?>
				<div style="padding: 20px;">
					<?php echo "<b>max_additional_life : " .$obj->products[$i]->max_additional_life."</b>";?> <br /><br />
					<!-- Option -->
					<input type="radio" class="product_code" id="product_code_<?php echo $i;?>" name="product_code" value="<?php echo $obj->products[$i]->product_code;?>"> <?php echo $obj->products[$i]->product_name;?><br />
						
						<div style="padding-left: 20px;">
						<?php $length_options = sizeof($obj->products[$i]->options);?>
						<?php if($length_options > 0) { ?>
						
							<?php for($a=0;$a<$length_options;$a++) {?>
								<?php if ($a==0) { $option = 'Y'; } elseif ($a==1) { $option = 'N';} ?>
								<div>
								<!-- Sub option -->
								<input type="radio" class="radio_op_code" name="options[0][op_code]" value="<?php echo $obj->products[$i]->options[$a]->op_code; ?>" /> <?php echo $obj->products[$i]->options[$a]->op_name; ?>
								<!-- <input type="text" class="op_value" name="options[0][op_value]" value="<?php echo $option;?>" readonly="readonly" />  -->
								</div>
							<?php } ?>
						<?php } ?>
						</div>
						<hr />
				</div>
				<?php } ?> <!-- end of for -->
			</div>
			<button type="button" class="btn btn-success" id="btn_choose_product">Next</button>
		</div>
		
		<input type="hidden" class="op_code" name="options[0][op_code]" value="" /> 
		<input type="hidden" class="op_value" name="options[0][op_value]" value="" />
		
		<!-- Cust info -->
		<div class="container cust_info"> 
			<h1>Cust Info</h1>
			<button type="button" class="btn btn-primary btn-add-cust-info">Add Customer Info</button> (Button ini dapat digunakan berdasarkan pilihan max_additional_life di awal (getProduct)).
			<hr />
			
			<table id="cust_info_1">
				<tr>
					<th>Policy Holder's Profile : </th>
				</tr>
				<tr>
					<td>Date of Birth</td>
					<td> : </td>
					<td> <input type="text" name="cust_info[0][dob]" class="form-control" placeholder ="YYYY-MM-DD"  /> </td>
				</tr>
				<tr>
					<td> Gender</td>
					<td> : </td>
					<td>
						<input type="radio" name="cust_info[0][gender_code]" value="M" /> Male
						<input type="radio" name="cust_info[0][gender_code]" value="F" /> Female
					</td>
				</tr>
				<tr>
					<td><label>LIFE_ASSURED_RELATIONSHIP (rel_code) </label></td>
					<td> : </td>
					<td>
						
						<select name="cust_info[0][rel_code]" class="form-control">
							<option value="SE" selected="selected">null</option>
							<option value="SE">SELF</option>
							<option value="SP">SPOUSE</option>
							<option value="CH">CHILDREN</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label>ROLE (role_code)</label></td>
					<td>:</td>
					<td>
						<select name="cust_info[0][role_code]" class="form-control">
							<option value="2">Beneficiary</option>
							<option value="4" selected="selected">Policy Holder</option>
							<option value="8">Life Assured</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label>Life Index (life_index)</label></td>
					<td>:</td>
					<td>
						<select name="cust_info[0][life_index]" class="form-control">
							<option value="null" selected="selected">null</option>
							<?php for($life_index=1;$life_index <= 9; $life_index++) { ?>
							<option value="<?php echo $life_index?>"><?php echo $life_index;?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				 
			</table>
			<table id="cust_info_2">
				<tr>
					<th>Life assured Profile : </th>
				</tr>
				<tr>
					<td> Date of Birth</td>
					<td> : </td>
					<td> <input type="text" name="cust_info[1][dob]" class="form-control" placeholder ="YYYY-MM-DD"  /> </td>
				</tr>
				<tr>
					<td> Gender</td>
					<td> : </td>
					<td>
						<input type="radio" name="cust_info[1][gender_code]" value="M" /> Male
						<input type="radio" name="cust_info[1][gender_code]" value="F" /> Female
					</td>
				</tr>
				<tr>
					<td><label>LIFE_ASSURED_RELATIONSHIP (rel_code) </label></td>
					<td> : </td>
					<td>
						
						<select name="cust_info[1][rel_code]" class="form-control">
							<option value="SE">null</option>
							<option value="SE">SELF </option>
							<option value="SP">SPOUSE </option>
							<option value="CH">CHILDREN </option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label>ROLE (role_code)</label></td>
					<td>:</td>
					<td>
						<select name="cust_info[1][role_code]" class="form-control">
							<option value="2">Beneficiary </option>
							<option value="4">Policy Holder </option>
							<option value="8">Life Assured </option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label>Life Index (life_index)</label></td>
					<td>:</td>
					<td>
						<select name="cust_info[1][life_index]" class="form-control">
							<option value="null">null </option>
							<?php for($life_index=1;$life_index <= 9; $life_index++) { ?>
							<option value="<?php echo $life_index?>"><?php echo $life_index;?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				 
			</table>
			<table id="cust_info_submit">
				<tr>
					<td> <input type="submit" class="btn btn-success" value="Next" /> </td>
				</tr>
			</table>
		</div>	
			
			
		<?php echo form_close();?>
	</body>
	<script>
		$(document).ready(function() {
			var product_code = '';
			
			$("#btn_choose_product").click(function(){
				$(".choose_product").hide();
				$(".cust_info").show();
			});

			$('.product_code').click(function(){
				product_code = $(this).attr('id');
				
			});
			
			$('#btn_choose_product').click(function(){
				console.log(product_code);
				if (product_code == 'product_code_2') {
					$('.op_code').prop('disabled', true);
					$('.op_value').prop('disabled', true);
				} else {
					$('.op_code').prop('disabled', false);
					$('.op_value').prop('disabled', false);
				}
			});
			$(".radio_op_code").on('click', function(){
				var op_code = $(this).val();
				console.log(op_code);
				
				if (op_code == "ROP1") {
					$(".op_code").val("ROP1");
					$(".op_value").val("Y");
				} else if (op_code == "ROP2") {
					$(".op_code").val("ROP2");
					$(".op_value").val("N");
				} 
				
			});

			var i = 2;
			var profile_count = 2;
			$(".btn-add-cust-info").click(function() {
			
				var cust_info = '<tr>';
					cust_info += '<th>Profile '+profile_count+'</th>';
					cust_info += '</tr>';

					cust_info += '<tr>';
					cust_info += '<td> Date of Birth</td>';
					cust_info += '<td> : </td>';
					cust_info += 	'<td> <input type="text" name="cust_info['+i+'][dob]" class="form-control" placeholder ="YYYY-MM-DD"  /> </td>';
					cust_info += '</tr>';

					cust_info += '<tr>';
					cust_info += '<td> Gender</td>';
					cust_info += '<td> : </td>';
					cust_info += '<td>' 
					cust_info += 	'<input type="radio" name="cust_info['+i+'][gender_code]" value="M" /> Male';
					cust_info += 	'<input type="radio" name="cust_info['+i+'][gender_code]" value="F" /> Female';
					cust_info += '</td>';	
					cust_info += '</tr>';

					cust_info += '<tr>';
					cust_info += '<td><label>LIFE_ASSURED_RELATIONSHIP (rel_code) </label></td>';
					cust_info += '<td> : </td>';
					cust_info += '<td>'; 
					cust_info += '<select name="cust_info['+i+'][rel_code]" class="form-control">';
					cust_info += 	'<option value="SE">null</option>';
					cust_info += 	'<option value="SE">SELF </option>';
					cust_info += 	'<option value="SP">SPOUSE </option>';
					cust_info += 	'<option value="CH">CHILDREN </option>';
					cust_info += '</select>';
					cust_info += '</td>';	
					cust_info += '</tr>';

					cust_info += '<tr>';
					cust_info += '<td><label>ROLE (role_code)</label></td>';
					cust_info += '<td> : </td>';
					cust_info += '<td>';
					cust_info += '<select name="cust_info['+i+'][role_code]" class="form-control">';
					cust_info += 	'<option value="2">Beneficiary</option>';
					cust_info += 	'<option value="4">Policy Holder </option>';
					cust_info += 	'<option value="8">Life Assured </option>';
					cust_info += '</select>';
					cust_info += '</td>';	
					cust_info += '</tr>';


					cust_info += '<tr>';
					cust_info += '<td><label>Life Index (life_index)</label></td>';
					cust_info += '<td>:</td>';
					cust_info += '<td>';	
					cust_info += '<select name="cust_info['+i+'][life_index]" class="form-control">';
					cust_info += '<option value="null">null </option>';
					cust_info += '<?php for($life_index=1;$life_index <= 9; $life_index++) { ?>';
					cust_info += '<option value="<?php echo $life_index?>"><?php echo $life_index;?></option>';
					cust_info += '<?php } ?>';
					cust_info += '</select>';
					cust_info += '</td>';	
			    $("#cust_info_2").append(cust_info);
			    profile_count +=1;
			    i+=1;

				//var success_alert = '<div class="alert alert-success">Profile has been added!</div>';
				//$(".message").append(success_alert);
				
			    alert('New profile form has been added!');
			});
		});
	</script>
</html>